#!/usr/bin/env python3
# TODO: Use the youtube_dl api instead of calling the cli.
from subprocess import Popen, PIPE
from multiprocessing import Pool

songs = [
    ("Turkish March, Mozart", "https://youtu.be/HMjQygwPI1c"),
    ("Beethoven's silence, Ernesto Cortazar", "https://youtu.be/YFD2PPAqNbw"),
    ("Dmitri Shostakovich - Waltz No. 2", "https://youtu.be/mmCnQDUSO4I"),
    ("Beethoven-Fur Elise", "https://youtu.be/k_UOuSklNL4")
]

# title is stdout or stderr.
def printout(title, body):
    if not title or not body:
        print("%s: empty" % title)
    else:
        sep = "-" * 20
        print(sep + " " + title + " " + sep)
        print(body.decode("utf-8"))

def download(tup):
    (title, link) = tup
    print("Downloading %s..." % title)
    cmd = Popen([
        "youtube-dl", "--add-metadata", "-ic", "-x", "-f", "bestaudio/best",
        link, "-o", "%s.%%(ext)s" % title], stdout = PIPE, stderr = PIPE)

    stdout, stderr = cmd.communicate()

    if cmd.returncode != 0:
        print("Failed to download %s! (youtube-dl exit code %d)" % (title, cmd.returncode))
        printout("stdout", stdout)
        printout("stderr", stderr)

with Pool(len(songs)) as p:
    p.map(download, songs)
